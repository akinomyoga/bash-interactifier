set -e

bash oshinstall.sh --unattended
git submodule update --init --recursive --recommend-shallow --depth 1
cd ble.sh
make
cd ..
blesh_plugin_dir=~/.oh-my-bash/plugins/blesh
mkdir "$blesh_plugin_dir"
cp -r ble.sh/out/* "$blesh_plugin_dir/"
printf "source '%s'\n" "$blesh_plugin_dir/ble.sh" > "$blesh_plugin_dir/blesh.plugin.sh"
cp .bashrc ~/.bashrc
